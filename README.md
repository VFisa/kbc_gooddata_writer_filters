# README #

Custom app in python for Keboola Connection Platform (KBC)

### What is this repository for? ###

* Get filter IDs for users in supplied table
* Filters are for KBC Gooddata Writer

### How do I get set up? ###

Configuration:
```
{
  "x-storageapi-token": "STORAGE TOKEN",
  "file": "/data/in/tables/data.csv"
}
```

### Limitations
* Note: make sure your token has an access rights for Gooddata writer and bucket in storage
* Please mind known limitations (time limit) for custom apps in KBC. It should be fine for <3500 emails though
* Error emails (possibly missing) are captured in the LOG

### Contact ###

* @VFisa
* fisa@keboola.com