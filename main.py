"__author__ = 'Martin Fiser'"
"__credits__ = 'Keboola 2016, Twitter: @VFisa'"

from keboola import docker
import requests
import json
import csv
#import pprint
import re



# initialize application
cfg = docker.Config("/data/")
params = cfg.get_parameters()

# access the supplied values
TOKEN = cfg.get_parameters()["x-storageapi-token"]
file = cfg.get_parameters()["file"]


def data_assembly(file):
    """
    1. open data.csv and parse it
    2. call request for each email
    3. assemble data together
    """
    data = {}
    alist = []

    ## open file
    f = open(file)
    content = f.read()
    f.close()
    ## lines excercise
    words = content.split('\n')
    words = words[1:]
    emails = []
    for a in words:
        email = a.replace('"','')
        emails.append(email)
    #print emails

    ## call request for each email and merge the output with the final data
    for b in emails:
        #print b
        user_filter = {}
        ## request data
        user_filter = get_filters(b)
        ## update final dictionary
        data.update(user_filter)
    
    #print data
    return data


def get_filters(email):
    """
    Get the list of filters for defined users
    """
    
    URL = str("https://syrup.keboola.com/gooddata-writer/v2/Main/users/"+email+"/filters")
    
    HEADERS = {
        "x-storageapi-token": TOKEN
        ,"cache-control": "no-cache"
        #,"postman-token": "71ab4ff6-9085-5d0e-12b7-64e73ebf9553"
        }

    PAYLOAD = {
        'config':'Main'
        ,'email': email
        }

    print "Sending request.."+email
    response = requests.request("GET", URL, headers=HEADERS, params=PAYLOAD)
    
    ## KBC logger should be done better, another area for further improvements
    print ("Response status code: "+str(response.status_code))
    #pprint.pprint(PAYLOAD)
    #print(response.url)
    #pprint.pprint(response.text)
    
    ## Dictionarize output, take only good ones
    user_filter = {}
    if response.status_code == 200:
        dataRaw = json.loads(response.text)
        for a in dataRaw:
            user_filter[str(a)] = email
        #pprint.pprint(user_filter)
    else:
        print "ERROR: "+str(email)
    
    return user_filter


def write_data(data):
    """
    temporarily save output table
    data is a dictionary type
    """

    data_file = open('/data/out/tables/data.csv', 'w')
    csvwriter = csv.writer(data_file)

    ## Header
    csvwriter.writerow(["filter", "user"])
    ## Save data as CSV
    for key, value in data.items():
        csvwriter.writerow([key, value])
    data_file.close()

    print "Job done."


## read imput table
data = data_assembly(file)

## write output
write_data(data)
